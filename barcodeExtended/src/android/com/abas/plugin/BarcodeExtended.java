package com.abas.plugin;

import java.util.Iterator;
import java.util.Map;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.os.SystemClock;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.dimo.PayByQR.*;
import com.dimo.PayByQR.PayByQRSDKListener;
import com.dimo.PayByQR.UserAPIKeyListener;
import com.dimo.PayByQR.model.InvoiceModel;
import com.dimo.PayByQR.model.LoyaltyModel;

public class BarcodeExtended extends CordovaPlugin implements PayByQRSDKListener {
	public PayByQRSDK payByQRSDK;
	public InvoiceModel invoiceModel=new InvoiceModel();
	public String userApiKey;
	public boolean responseAcepted=false;
	private CallbackContext cbCtx;
	JSONObject jsonObj = new JSONObject();
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
	}

	private void openNewActivity(Context context, String userKey) {
		Intent intent = new Intent(context, NewActivity.class);
		this.cordova.getActivity().startActivity(intent);
	}

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if ("coolMethod".equals(action)) {
			Context context = this.cordova.getActivity().getApplicationContext();

			this.userApiKey = args.getString(0);
			this.payByQRSDK = new PayByQRSDK(this.cordova.getActivity(), this);
			this.payByQRSDK.setServerURL(PayByQRSDK.ServerURL.SERVER_URL_DEV);
			this.payByQRSDK.setEULAState(true);
			this.payByQRSDK.setMinimumTransaction(1);
			this.payByQRSDK.setIsUsingCustomDialog(false);
			this.payRequest(this.userApiKey, callbackContext);
			// String
			//cicak=String.valueOf(PayByQRSDK.MODULE_PAYMENT);//PayByQRSDK.ServerURL.SERVER_URL_DEV
			//echo(cicak, callbackContext);
			return true;
		} else if ("coolMethod2".equals(action)) {
			Context context = cordova.getActivity().getApplicationContext();
			this.openNewActivity(context, args.getString(0));
			
			return true;
		}

		return false;
	}

	private void payRequest(
    String uKey, 
    CallbackContext callbackContext
  ) {
   if (uKey == null || uKey.length() == 0) {
      callbackContext.error("Key is Empty");
   }else {
	  this.payByQRSDK.startSDK(PayByQRSDK.MODULE_PAYMENT);
	  this.cbCtx=callbackContext;
	  

    } 
  }

	@Override
	public void callbackActivityResult(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackAuthenticationError() {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackCustomQR(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackEULAStateChanged(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void callbackGenerateUserAPIKey(UserAPIKeyListener arg0) {
		// TODO Auto-generated method stub
		arg0.setUserAPIKey(this.userApiKey);

	}

	@Override
	public boolean callbackInvalidQRCode() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void callbackLostConnection() {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackPayInvoice(InvoiceModel arg0) {
		// TODO Auto-generated method stub
		//this.invoiceModel = new InvoiceModel();
		
		this.invoiceModel = arg0;
		try {		  
			 
			  this.jsonObj.put("invoiceID", arg0.invoiceID); 
			  this.jsonObj.put("merchantName", arg0.merchantName); 
			  this.jsonObj.put("originalAmount", arg0.originalAmount); 
			  this.jsonObj.put("paidAmount", arg0.paidAmount); 
			  this.jsonObj.put("amountOfDiscount", arg0.amountOfDiscount); 
			  this.jsonObj.put("discountType", arg0.discountType); 
			  this.jsonObj.put("numberOfCoupons", arg0.numberOfCoupons); 
			  this.jsonObj.put("loyaltyProgramName", arg0.loyaltyProgramName); 
			  this.jsonObj.put("tipAmount", arg0.tipAmount); 
			  this.jsonObj.put("pointsRedeemed", arg0.pointsRedeemed);  
			  this.jsonObj.put("amountRedeemed", arg0.amountRedeemed);
			  
			  
			  }
			  catch(JSONException ex) {
				  ex.printStackTrace();
				  }

		this.payByQRSDK.closeSDK();
		this.cbCtx.success(this.jsonObj);

	}

	@Override
	public void callbackSDKClosed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void callbackShowDialog(Context arg0, int arg1, String arg2, LoyaltyModel arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public Fragment callbackShowEULA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean callbackTransactionStatus(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean callbackUnknowError() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void callbackUserHasCancelTransaction() {
		// TODO Auto-generated method stub

	}
}
