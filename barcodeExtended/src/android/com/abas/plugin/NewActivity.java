package com.abas.plugin;

import android.app.Activity;
import java.util.Iterator;
import java.util.Map;

import android.support.design.widget.FloatingActionButton; 
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.dimo.PayByQR.*;
import com.dimo.PayByQR.PayByQRSDKListener; 
import com.dimo.PayByQR.UserAPIKeyListener; 
import com.dimo.PayByQR.model.InvoiceModel;
import com.dimo.PayByQR.model.LoyaltyModel;

public class NewActivity extends Activity implements PayByQRSDKListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String package_name = getApplication().getPackageName();
        //setContentView(getApplication().getResources().getIdentifier("activity_new", "layout", package_name));
        PayByQRSDK payByQRSDK = new PayByQRSDK(this,this);
    	payByQRSDK.setServerURL(PayByQRSDK.ServerURL.SERVER_URL_UAT);
    	payByQRSDK.setEULAState(true);
    	payByQRSDK.setMinimumTransaction(1000);
    	payByQRSDK.setIsUsingCustomDialog(false);
    	payByQRSDK.startSDK(PayByQRSDK.MODULE_PAYMENT);
    }
    
    @Override
   	public void callbackActivityResult(int arg0, int arg1, Intent arg2) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackAuthenticationError() {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackCustomQR(String arg0) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackEULAStateChanged(boolean arg0) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackGenerateUserAPIKey(UserAPIKeyListener arg0) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public boolean callbackInvalidQRCode() {
   		// TODO Auto-generated method stub
   		return false;
   	}

   	@Override
   	public void callbackLostConnection() {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackPayInvoice(InvoiceModel arg0) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackSDKClosed() {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public void callbackShowDialog(Context arg0, int arg1, String arg2, LoyaltyModel arg3) {
   		// TODO Auto-generated method stub
   		
   	}

   	@Override
   	public Fragment callbackShowEULA() {
   		// TODO Auto-generated method stub
   		return null;
   	}

   	@Override
   	public boolean callbackTransactionStatus(int arg0, String arg1) {
   		// TODO Auto-generated method stub
   		return false;
   	}

   	@Override
   	public boolean callbackUnknowError() {
   		// TODO Auto-generated method stub
   		return false;
   	}

   	@Override
   	public void callbackUserHasCancelTransaction() {
   		// TODO Auto-generated method stub
   		
   	}
}
